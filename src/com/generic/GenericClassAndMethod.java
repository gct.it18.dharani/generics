package com.generic;

public class GenericClassAndMethod<type> {
    type content;
    GenericClassAndMethod(type content){
        this.content = content;
    }

    public void setContent(type content) {
        this.content = content;
    }

    public type getContent() {
        return content;
    }

    public <type> void genericMethod(type[] arrayElements){
        System.out.println("Array Elements are ");
        for (type i: arrayElements){
            System.out.println(i);
        }
    }
}
