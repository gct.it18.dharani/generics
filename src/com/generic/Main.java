package com.generic;

public class Main {

    public static void main(String[] args) {

        GenericClassAndMethod genericClassAndMethod1 = new GenericClassAndMethod("Generic");
        System.out.println(genericClassAndMethod1.getContent());

        GenericClassAndMethod genericClassAndMethod2 = new GenericClassAndMethod<>(1000);
        System.out.println(genericClassAndMethod2.getContent());

        String[] str = {"One", "Two", "Three"};
        genericClassAndMethod1.genericMethod(str);
    }
}
